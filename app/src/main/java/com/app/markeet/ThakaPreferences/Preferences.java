package com.app.markeet.ThakaPreferences;

import android.content.Context;
import android.content.ContextWrapper;

import com.pixplicity.easyprefs.library.Prefs;

public class Preferences {
    public Preferences(Context context){
        new Prefs.Builder()
                .setContext(context)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(context.getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();
    }

    public void setID(String id){
        Prefs.putString("id",id);
    }
    public String getId(){
        return Prefs.getString("id","");
    }

    public void setSession(Boolean session){
        Prefs.putBoolean("session",session);
    }

    public Boolean getSession(){
        return Prefs.getBoolean("session",false);
    }

    public void clear() {
        Prefs.clear();
    }

    public void setEmail(String email) {
        Prefs.putString("email",email);
    }

    public void setNumber(String number) {
        Prefs.putString("number",number);
    }

    public void setName(String name) {
        Prefs.putString("name",name);
    }


    public String getEmail(){
        return Prefs.getString("email","");
    }

    public String getName(){
        return Prefs.getString("name","");
    }
    public String getNumber(){
        return Prefs.getString("number","");
    }
}
