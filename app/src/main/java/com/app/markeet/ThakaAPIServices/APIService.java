package com.app.markeet.ThakaAPIServices;

import com.app.markeet.ThakaModels.CategoryModel;
import com.app.markeet.ThakaModels.Custom_Product_chec;
import com.app.markeet.ThakaModels.LoginModel;
import com.app.markeet.ThakaModels.OrderAPIMOdel;
import com.app.markeet.ThakaModels.ProductDetailModel;
import com.app.markeet.ThakaModels.ProductFeatureModel;
import com.app.markeet.ThakaModels.SearchCategoryModel;
import com.app.markeet.ThakaModels.SignupModel;
import com.app.markeet.ThakaModels.SingleProductDetailModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIService {

    @GET("category")
    Call<CategoryModel> Category();

    @GET("product/by/category/{productid}")
    Call<ProductDetailModel> ProductCall(@Path("productid") String id);

    @GET("product/by/featur")
    Call<ProductFeatureModel> FEATURE_MODEL_CALL();

    @GET("product/{product_id}")
    Call<SingleProductDetailModel> SINGLE_PRODUCT_DETAIL_MODEL_CALL(@Path("product_id")String id);

    @FormUrlEncoded
    @POST("category/search")
    Call<CategoryModel> SearchCategory(@Field("search") String search_data);

    @FormUrlEncoded
    @POST("product/search")
    Call<ProductDetailModel> SearchProduct(@Field("search") String toString,@Field("category") String id);

    @FormUrlEncoded
    @POST("customer/signup")
    Call<SignupModel> Signup(@Field("email") String email,@Field("name") String name,@Field("number") String number);

    @FormUrlEncoded
    @POST("customer/login")
    Call<LoginModel> LOGIN(@Field("number") String getNumber);

    @FormUrlEncoded
    @POST("order")
    Call<OrderAPIMOdel> ORDER_APIM_ODEL_CALL(@Field("products") ArrayList<String> products,
                                             @Field("packages")ArrayList<String> pkg,
                                             @Field("customer")String customer_id,
                                             @Field("total_amount")String amount,
                                             @Field("shipping_address")String address,
                                             @Field("comment")String comment,
                                             @Field("required_date")String required_date);

   /* @GET("order")
    Call<OrderTrackingModel>*/
}
