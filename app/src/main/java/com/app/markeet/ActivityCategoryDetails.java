package com.app.markeet;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import com.google.android.material.appbar.AppBarLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.markeet.ThakaAPIServices.APIClient;
import com.app.markeet.ThakaAPIServices.APIService;
import com.app.markeet.ThakaModels.CategoryModel;
import com.app.markeet.ThakaModels.ProductDetailModel;
import com.app.markeet.adapter.AdapterProduct;
import com.app.markeet.connection.callbacks.CallbackProduct;
import com.app.markeet.model.Category;
import com.app.markeet.model.Product;
import com.app.markeet.utils.Tools;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityCategoryDetails extends AppCompatActivity {
    private static final String EXTRA_OBJECT = "key.EXTRA_OBJECT";

    // activity transition
    public static void navigate(Context context, CategoryModel.Body obj) {
        Intent i = new Intent(context, ActivityCategoryDetails.class);

        ArrayList<CategoryModel.Body> arrayList=new ArrayList<>();
        arrayList.add(obj);
        Gson gson = new Gson();
        Type type = new TypeToken<CategoryModel.Body>() {
        }.getType();

        String json = gson.toJson(obj, type);
        i.putExtra(EXTRA_OBJECT, json);
        context.startActivity(i);
    }

    // extra obj
    private Category category;

    private Toolbar toolbar;
    private ActionBar actionBar;
    private View parent_view;
    private SwipeRefreshLayout swipe_refresh;
    private Call<CallbackProduct> callbackCall = null;

    private RecyclerView recyclerView;
    private AdapterProduct mAdapter;

    private int post_total = 0;
    private int failed_page = 0;
    CategoryModel.Body body;
    String id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_details);
        parent_view = findViewById(android.R.id.content);
        //

        initComponent();
        initToolbar();
        try {

         //   category = (Category) getIntent().getSerializableExtra(EXTRA_OBJECT);

            Gson gson = new Gson();
            Intent intent = getIntent();
            String stringLocation = intent.getStringExtra(EXTRA_OBJECT);

            String abc = stringLocation.toString().replace("[", "").replace("]", "");

            if (abc != null) {
                Type type = new TypeToken<CategoryModel.Body>() {
                }.getType();

               body  = gson.fromJson(abc, type);
                id=body.getId();
                displayCategoryData(body);
            } else {
                Log.e("Location Count", "failed");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        requestAction(body.getId());

        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                requestAction(body.getId());
            }
        });



    }

    private void initComponent() {
        swipe_refresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(this, Tools.getGridSpanCount(this)));
        recyclerView.setHasFixedSize(true);

        //set data and list adapter

        // on item list clicked
    /*    mAdapter.setOnItemClickListener(new AdapterProduct.OnItemClickListener() {
            @Override
            public void onItemClick(View v, Product obj, int position) {
                ActivityProductDetails.navigate(ActivityCategoryDetails.this, obj.id, false);
            }
        });*/

        // detect when scroll reach bottom
     /*   mAdapter.setOnLoadMoreListener(new AdapterProduct.OnLoadMoreListener() {
            @Override
            public void onLoadMore(int current_page) {
                if (post_total > mAdapter.getItemCount() && current_page != 0) {
                    int next_page = current_page + 1;
                    requestAction(next_page);
                } else {
                    mAdapter.setLoaded();
                }
            }
        });

        // on swipe list
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (callbackCall != null && callbackCall.isExecuted()) callbackCall.cancel();
                mAdapter.resetListData();
                requestAction(1);
            }
        });*/
    }

    private void displayCategoryData(CategoryModel.Body c) {

        ((TextView) findViewById(R.id.name)).setText(c.getTitle().getEnglish());
        ((TextView) findViewById(R.id.brief)).setText(c.getDescription().getEnglish());
        ImageView icon = (ImageView) findViewById(R.id.icon);
        ((AppBarLayout) findViewById(R.id.app_bar_layout)).setBackgroundColor(Color.parseColor(c.getColor()));
      //  Tools.displayImageOriginal(this, icon, Constant.getURLimgCategory(c.getImage().getThumbnail()));

        Tools.setSystemBarColorDarker(this, c.getColor());
        Glide.with(this).load(c.getImage().getThumbnail()).into(icon);


      /*  if (AppConfig.TINT_CATEGORY_ICON) {
            icon.setColorFilter(Color.WHITE);
        }*/

        // analytics track
      //  ThisApplication.getInstance().saveLogEvent(Long.parseLong(c.getId()), c.getTitle().getEnglish(), "CATEGORY_DETAILS");
    }

EditText product_search_box;
    Boolean visible_search=false;
    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        product_search_box=toolbar.findViewById(R.id.product_search_box);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("");

        product_search_box.setOnKeyListener(new View.OnKeyListener()
        {
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if (event.getAction() == KeyEvent.ACTION_DOWN)
                {
                    switch (keyCode)
                    {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                           toolbar.setVisibility(View.VISIBLE);
                            product_search_box.setVisibility(View.GONE);
                            callAPI(product_search_box.getText().toString());
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_category_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            super.onBackPressed();
        } else if (item_id == R.id.action_search) {
         //   ActivitySearch.navigate(ActivityCategoryDetails.this, category);
            callSearchProduct();
        } else if (item_id == R.id.action_cart) {
            Intent i = new Intent(this, ActivityShoppingCart.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    private void callSearchProduct() {
        if (visible_search){
            product_search_box.setVisibility(View.GONE);
            toolbar.setVisibility(View.VISIBLE);
            callAPI(product_search_box.getText().toString());
        }else {
            toolbar.setVisibility(View.GONE);
            product_search_box.setVisibility(View.VISIBLE);
        }
    }

    private void callAPI(String toString) {
        if (TextUtils.isEmpty(toString) && toString.equalsIgnoreCase("")) {
            Toast.makeText(this, "Nothing to search", Toast.LENGTH_SHORT).show();
        } else {
            APIService service = APIClient.getClient().create(APIService.class);
            Call<ProductDetailModel> modelCall = service.SearchProduct(toString,id);
            modelCall.enqueue(new Callback<ProductDetailModel>() {
                @Override
                public void onResponse(Call<ProductDetailModel> call, Response<ProductDetailModel> response) {
                    if (response.isSuccessful()){
                        ProductDetailModel model=response.body();

                        if (model.getStatus()){
                            ArrayList<ProductDetailModel.Body> arrayList=new ArrayList<>();
                            arrayList=model.getBody();
                            mAdapter = new AdapterProduct(ActivityCategoryDetails.this, arrayList);
                            recyclerView.setAdapter(mAdapter);
                            swipe_refresh.setRefreshing(false);

                        }else {
                            Toast.makeText(ActivityCategoryDetails.this, "No Data found", Toast.LENGTH_SHORT).show();
                            swipe_refresh.setRefreshing(false);
                        }


                    }else {
                        Toast.makeText(ActivityCategoryDetails.this, "Network Error", Toast.LENGTH_SHORT).show();
                        swipe_refresh.setRefreshing(false);
                    }
                }

                @Override
                public void onFailure(Call<ProductDetailModel> call, Throwable t) {
                    Toast.makeText(ActivityCategoryDetails.this, "Network Error", Toast.LENGTH_SHORT).show();
                    swipe_refresh.setRefreshing(false);
                    Log.e("error", "onFailure: "+t.getMessage() );
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    private void displayApiResult(final List<Product> items) {
      //  mAdapter.insertData(items);
       // swipeProgress(false);
        if (items.size() == 0) showNoItemView(true);
    }

    private void requestListProduct(final String id) {
      /*  API api = RestAdapter.createAPI();
        callbackCall = api.getListProduct(page_no, Constant.PRODUCT_PER_REQUEST, null, category.id);
        callbackCall.enqueue(new Callback<CallbackProduct>() {
            @Override
            public void onResponse(Call<CallbackProduct> call, Response<CallbackProduct> response) {
                CallbackProduct resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    post_total = resp.count_total;
                    displayApiResult(resp.products);
                } else {
                    onFailRequest(page_no);
                }
            }

            @Override
            public void onFailure(Call<CallbackProduct> call, Throwable t) {
                if (!call.isCanceled()) onFailRequest(page_no);
            }

        });*/
        APIService service= APIClient.getClient().create(APIService.class);
        Call<ProductDetailModel> modelCall=service.ProductCall(id);
        modelCall.enqueue(new Callback<ProductDetailModel>() {
            @Override
            public void onResponse(Call<ProductDetailModel> call, Response<ProductDetailModel> response) {
                if (response.isSuccessful()){
                    ProductDetailModel model=response.body();

                    if (model.getStatus()){
                        ArrayList<ProductDetailModel.Body> arrayList=new ArrayList<>();
                        arrayList=model.getBody();
                        mAdapter = new AdapterProduct(ActivityCategoryDetails.this, arrayList);
                        recyclerView.setAdapter(mAdapter);
                        swipe_refresh.setRefreshing(false);

                    }else {
                        Toast.makeText(ActivityCategoryDetails.this, "No Data found", Toast.LENGTH_SHORT).show();
                        swipe_refresh.setRefreshing(false);
                    }


                }else {
                    Toast.makeText(ActivityCategoryDetails.this, "Network Error", Toast.LENGTH_SHORT).show();
                    swipe_refresh.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(Call<ProductDetailModel> call, Throwable t) {
                Toast.makeText(ActivityCategoryDetails.this, "Network Error", Toast.LENGTH_SHORT).show();
                swipe_refresh.setRefreshing(false);
                Log.e("error", "onFailure: "+t.getMessage() );
            }
        });

    }

   /* private void onFailRequest(int page_no) {
        failed_page = page_no;
        mAdapter.setLoaded();
        swipeProgress(false);
        if (NetworkCheck.isConnect(this)) {
            showFailedView(true, getString(R.string.failed_text));
        } else {
            showFailedView(true, getString(R.string.no_internet_text));
        }
    }*/

    private void requestAction(final String id) {
        showFailedView(false, "");
        showNoItemView(false);
      /*  if (page_no == 1) {
            swipeProgress(true);
        } else {
            mAdapter.setLoading();
        }*/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestListProduct(id);
            }
        }, 1000);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
       // swipeProgress(false);
        if (callbackCall != null && callbackCall.isExecuted()) {
            callbackCall.cancel();
        }
    }

    private void showFailedView(boolean show, String message) {
        View lyt_failed = (View) findViewById(R.id.lyt_failed);
        ((TextView) findViewById(R.id.failed_message)).setText(message);
        if (show) {
            recyclerView.setVisibility(View.GONE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        /*((Button) findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction(failed_page);
            }
        });*/
    }

    private void showNoItemView(boolean show) {
        View lyt_no_item = (View) findViewById(R.id.lyt_no_item);
        if (show) {
            recyclerView.setVisibility(View.GONE);
            lyt_no_item.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            lyt_no_item.setVisibility(View.GONE);
        }
    }

    /*private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            return;
        }
        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(show);
            }
        });
    }*/
}
