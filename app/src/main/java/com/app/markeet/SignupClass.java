package com.app.markeet;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.hbb20.CountryCodePicker;

import java.util.concurrent.TimeUnit;

public class SignupClass extends AppCompatActivity {

    TextView login_text, email, name;
    Button button;
    Bundle bundle;
    Intent intent;
    String number;
    CountryCodePicker getNumber;
    EditText mobilenumber;
    private FirebaseAuth mAuth;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;


    private static final int STATE_INITIALIZED = 1;
    private static final int STATE_CODE_SENT = 2;
    private static final int STATE_VERIFY_FAILED = 3;
    private static final int STATE_VERIFY_SUCCESS = 4;
    private static final int STATE_SIGNIN_FAILED = 5;
    private static final int STATE_SIGNIN_SUCCESS = 6;

    private boolean mVerificationInProgress = false;
    private String mVerificationId;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.main_color));
        }

        setContentView(R.layout.activity_signup_class);


        login_text = findViewById(R.id.login_text);
        email = findViewById(R.id.email);
        name = findViewById(R.id.name);

        button = findViewById(R.id.button);
        FirebaseApp.initializeApp(this);
        mobilenumber = findViewById(R.id.mobilenumber);
        getNumber = findViewById(R.id.getNumber);
        getNumber.registerCarrierNumberEditText(mobilenumber);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(getResources().getString(R.string.please_wait));
        login_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                finish();
            }
        });
        mAuth = FirebaseAuth.getInstance();
        number = getNumber.getFullNumberWithPlus();

        intent = new Intent(getApplicationContext(), CodeActivity.class);

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                progressDialog.dismiss();
         //       Toast.makeText(SignupClass.this, phoneAuthCredential.get, Toast.LENGTH_SHORT).show();
                mVerificationInProgress = false;
                Toast.makeText(SignupClass.this, "Verification Complete", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                progressDialog.dismiss();
                   e.printStackTrace();
                Toast.makeText(SignupClass.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                progressDialog.dismiss();

                mResendToken = forceResendingToken;
                bundle=new Bundle();
                bundle.putString("verificationId", s);
                bundle.putParcelable("token", mResendToken);
                bundle.putString("phn_number", getNumber.getFullNumberWithPlus());
                bundle.putString("email", email.getText().toString());
                bundle.putString("name", name.getText().toString());
                bundle.putInt("otp_status", 3);
                intent.putExtras(bundle);
                updateUI(STATE_CODE_SENT);

            }
        };


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                String num=getNumber.getFullNumberWithPlus();
                String names=name.getText().toString();
                String emails=email.getText().toString();

                if (TextUtils.isEmpty(num) && TextUtils.isEmpty(names) && TextUtils.isEmpty(emails)){
                    Toast.makeText(SignupClass.this, "Please Fill all fields first", Toast.LENGTH_SHORT).show();
                }else {
                    startPhoneNumberVerification(getNumber.getFullNumberWithPlus());
                }
            }
        });

    }


    private void updateUI(int uiState) {
        switch (uiState) {
            case STATE_CODE_SENT:


                //Moving to code screen if code automatically not entered
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.otpCodeMsgRecieveMsg), Toast.LENGTH_SHORT).show();
                startActivity(intent);
                finish();

                break;
            case STATE_VERIFY_FAILED:
                // mDetailText.setText(R.string.status_verification_failed);
                break;
            case STATE_SIGNIN_FAILED:
                // mDetailText.setText(R.string.status_sign_in_failed);
                break;
            case STATE_SIGNIN_SUCCESS:
                break;
        }


    }

    private void startPhoneNumberVerification(String fullNumberWithPlus) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                fullNumberWithPlus,
                60,
                TimeUnit.SECONDS,
                this,
                mCallbacks);

        mVerificationInProgress = true;
    }
}
