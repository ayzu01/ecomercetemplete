package com.app.markeet.RoomDatabaseThaka.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.app.markeet.RoomDatabaseThaka.Entity.CartEntity;
import com.app.markeet.RoomDatabaseThaka.Repository.CartRepository;

import java.util.List;

public class CartViewModel extends AndroidViewModel {


    private CartRepository repository;
    private LiveData<List<CartEntity>> listLiveData;

    public CartViewModel(@NonNull Application application) {
        super(application);
        repository=new CartRepository(application);
        listLiveData=repository.getListLiveData();
    }

    public void insert(CartEntity cartEntity){
        repository.insert(cartEntity);
    }

    public LiveData<List<CartEntity>> getListLiveData(){
        return listLiveData;
    }
}
