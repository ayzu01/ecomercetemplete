package com.app.markeet.RoomDatabaseThaka;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.app.markeet.RoomDatabaseThaka.Dao.CartDAO;
import com.app.markeet.RoomDatabaseThaka.Entity.CartEntity;


@Database(entities = {CartEntity.class},version = 2)
public abstract class ThakaDatabase extends RoomDatabase {

    private static ThakaDatabase instance;

    public static synchronized ThakaDatabase getInstance(Context context){
        if (instance==null){
            instance= Room.databaseBuilder(context.getApplicationContext(),ThakaDatabase.class,"thaka_database")
                    .fallbackToDestructiveMigration().build();
        }
        return instance;
    }

    public abstract CartDAO CARTDAO();
}
