package com.app.markeet.RoomDatabaseThaka.Dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.app.markeet.RoomDatabaseThaka.Entity.CartEntity;

import java.util.List;

@Dao
public interface CartDAO {
    @Insert
    void insert(CartEntity list);

    @Query("Select * From cart_entity")
    LiveData<List<CartEntity>> getAllList();
}
