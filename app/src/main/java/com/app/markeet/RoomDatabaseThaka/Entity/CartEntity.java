package com.app.markeet.RoomDatabaseThaka.Entity;


import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Cart_Entity")
public class CartEntity {
    String image;
    String id;
    String price;
    String title;
    @PrimaryKey(autoGenerate = true)
    int main_id;

    public CartEntity(String image, String id, String price, String title) {
        this.image = image;
        this.id = id;
        this.price = price;
        this.title = title;
    }
    public void setMain_id(int main_id) {
        this.main_id = main_id;
    }

    public int getMain_id() {
        return main_id;
    }
    public String getImage() {
        return image;
    }

    public String getId() {
        return id;
    }

    public String getPrice() {
        return price;
    }

    public String getTitle() {
        return title;
    }
}
