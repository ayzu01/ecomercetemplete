package com.app.markeet.RoomDatabaseThaka.Repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.app.markeet.RoomDatabaseThaka.Dao.CartDAO;
import com.app.markeet.RoomDatabaseThaka.Entity.CartEntity;
import com.app.markeet.RoomDatabaseThaka.ThakaDatabase;

import java.util.List;

public class CartRepository {

    private CartDAO dao;
    private LiveData<List<CartEntity>> listLiveData;

    public CartRepository(Application application) {
        ThakaDatabase database = ThakaDatabase.getInstance(application);
        dao = database.CARTDAO();
        listLiveData = dao.getAllList();
    }

    public void insert(CartEntity listData) {
        new CartRepository.CartEntryAsync(dao).execute(listData);
    }

    public LiveData<List<CartEntity>> getListLiveData() {
        return listLiveData;
    }

    public class CartEntryAsync extends AsyncTask<CartEntity, Void, Void> {

        private CartDAO dao;

        public CartEntryAsync(CartDAO dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(CartEntity... cartEntities) {
            dao.insert(cartEntities[0]);
            return null;
        }
    }
}
