package com.app.markeet.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.markeet.R;
import com.app.markeet.model.Cart;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class CheckoutAdapter extends RecyclerView.Adapter<CheckoutAdapter.Holder> {
    Context context;
    private List<Cart> arrayList=new ArrayList<>();
    public CheckoutAdapter(Context context, List<Cart> arrayList){
        this.context=context;
        this.arrayList=arrayList;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(context).inflate(R.layout.item_checkout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.amount.setText(String.valueOf(arrayList.get(position).stock));
        holder.unit_price.setText(String.valueOf(arrayList.get(position).amount));
        holder.product_name.setText(arrayList.get(position).product_name);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public List<Cart> getItem() {
        return arrayList;
    }

    class Holder extends RecyclerView.ViewHolder {
        TextView product_name,unit_price,amount;
        Holder(@NonNull View itemView) {
            super(itemView);
            product_name=itemView.findViewById(R.id.product_name);
            unit_price=itemView.findViewById(R.id.unit_price);
            amount=itemView.findViewById(R.id.amounts);
        }
    }
}
