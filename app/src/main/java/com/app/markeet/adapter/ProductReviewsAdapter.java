package com.app.markeet.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.markeet.R;
import com.app.markeet.ThakaModels.SingleProductDetailModel;

import java.util.ArrayList;

public class ProductReviewsAdapter extends RecyclerView.Adapter<ProductReviewsAdapter.ViewHolder> {

    Context context;
    ArrayList<SingleProductDetailModel.Review> arrayList;

    public ProductReviewsAdapter(Context context, ArrayList<SingleProductDetailModel.Review> arrayList){
        this.arrayList=arrayList;
        this.context=context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.review_item,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.name.setText(arrayList.get(position).getCustomer().getName());
        holder.date.setText(arrayList.get(position).getAddedOn());
        holder.message_detail.setText(arrayList.get(position).getBody());
        holder.rating_bar.setRating(arrayList.get(position).getRating());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView message_detail,date,name;
        AppCompatRatingBar rating_bar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            message_detail=itemView.findViewById(R.id.message_detail);
            date=itemView.findViewById(R.id.date);
            rating_bar=itemView.findViewById(R.id.rating_bar);
            name=itemView.findViewById(R.id.name);
        }
    }
}
