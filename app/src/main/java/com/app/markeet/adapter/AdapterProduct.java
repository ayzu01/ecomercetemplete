package com.app.markeet.adapter;

import android.content.Context;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.markeet.ActivityProductDetails;
import com.app.markeet.R;
import com.app.markeet.ThakaModels.ProductDetailModel;
import com.app.markeet.data.SharedPref;
import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class AdapterProduct extends RecyclerView.Adapter<AdapterProduct.ProductViewHolder> {

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    private ArrayList<ProductDetailModel.Body> arrayList = new ArrayList<>();

    private boolean loading;
    // private OnLoadMoreListener onLoadMoreListener;

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;
    private SharedPref sharedPref;

    public interface OnItemClickListener {
        void onItemClick(View view, ProductDetailModel.Body obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterProduct(Context context, ArrayList<ProductDetailModel.Body> arrayList) {
        this.arrayList = arrayList;
        ctx = context;
        sharedPref = new SharedPref(ctx);
        // lastItemViewDetector(view);
    }

    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView name;
        public TextView price;
        public TextView price_strike;
        public ImageView image;
        public AppCompatRatingBar rating_bar;
        public MaterialRippleLayout lyt_parent;

        public OriginalViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.name);
            price = (TextView) v.findViewById(R.id.price_abc);
            price_strike = (TextView) v.findViewById(R.id.price_strike);
            image = (ImageView) v.findViewById(R.id.image);
            lyt_parent = (MaterialRippleLayout) v.findViewById(R.id.lyt_parent);
            rating_bar = v.findViewById(R.id.rating_bar);
        }
    }


   /* public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progress_loading);
        }
    }*/

   /* @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
     *//*   if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
            vh = new OriginalViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            vh = new ProgressViewHolder(v);
        }*//*
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }*/

    // Replace the contents of a view (invoked by the layout manager)
    /*@Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
       // if (holder instanceof OriginalViewHolder) {
            final ProductDetailModel.Body p = arrayList.get(position);
            OriginalViewHolder vItem = (OriginalViewHolder) holder;
        vItem.name.setText(p.getTitle().getEnglish());
            // handle discount view
            try {

                if (p.getDiscount() > 0) {
                    vItem.price.setText(p.getDiscount());
                    vItem.price_strike.setText(p.getPrice());
                    vItem.price_strike.setPaintFlags(vItem.price_strike.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    vItem.price_strike.setVisibility(View.VISIBLE);
                } else {
                    vItem.price.setText(p.getPrice());
                    vItem.price_strike.setVisibility(View.GONE);
                }
            //    Tools.displayImageOriginal(ctx, vItem.image, Constant.getURLimgProduct(p.getImages().get(0).getThumbnail()));

                Glide.with(ctx).load(p.getImages().get(0).getThumbnail()).into(vItem.image);
                vItem.rating_bar.setRating(p.getRating());
                vItem.lyt_parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ActivityProductDetails.navigate(ctx, Long.valueOf(p.getId()), p.getTitle().getEnglish(), false);
                    }
                });
           *//* vItem.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, p, position);
                    }
                }
            });*//*
            }catch (Exception e){
                e.printStackTrace();
            }
      *//*  }else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }*//*
    }*/

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ProductViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_product, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, final int i) {
        try {
            holder.name.setText(arrayList.get(i).getTitle().getEnglish());
            Glide.with(ctx).load(arrayList.get(i).getImages().get(0).getThumbnail()).into(holder.image);
            holder.rating_bar.setRating(arrayList.get(i).getRating());

            if (arrayList.get(i).getDiscount() > 0) {
                holder.price.setText(String.valueOf(arrayList.get(i).getDiscount()));
                holder.price_strike.setText(String.valueOf(arrayList.get(i).getPrice()));
                holder.price_strike.setPaintFlags(holder.price_strike.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder.price_strike.setVisibility(View.VISIBLE);
            } else {
                holder.price.setText(String.valueOf(arrayList.get(i).getPrice()));
                holder.price_strike.setVisibility(View.GONE);
            }
            //    Tools.displayImageOriginal(ctx, vItem.image, Constant.getURLimgProduct(p.getImages().get(0).getThumbnail()));

            holder.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ActivityProductDetails.navigate(ctx, arrayList.get(i).getId(), arrayList.get(i).getTitle().getEnglish(), false);
                }
            });
            holder.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityProductDetails.navigate(ctx, arrayList.get(i).getId(), arrayList.get(i).getTitle().getEnglish(), false);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public TextView price;
        public TextView price_strike;
        public ImageView image;
        public AppCompatRatingBar rating_bar;
        public MaterialRippleLayout lyt_parent;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.name);
            price = (TextView) itemView.findViewById(R.id.price_abc);
            price_strike = (TextView) itemView.findViewById(R.id.price_strike);
            image = (ImageView) itemView.findViewById(R.id.image);
            lyt_parent = (MaterialRippleLayout) itemView.findViewById(R.id.lyt_parent);
            rating_bar = itemView.findViewById(R.id.rating_bar);
        }
    }

    /*@Override
    public int getItemViewType(int position) {
      //  return this.arrayList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
        return
    }
*/
   /* public void insertData(List<Product> items) {
        setLoaded();
        int positionStart = getItemCount();
        int itemCount = items.size();
        this.arrayList.addAll(items);
        notifyItemRangeInserted(positionStart, itemCount);
    }*/

  /*  public void setLoaded() {
        loading = false;
        for (int i = 0; i < getItemCount(); i++) {
            if (items.get(i) == null) {
                items.remove(i);
                notifyItemRemoved(i);
            }
        }
    }*/

  /*  public void setLoading() {
        if (getItemCount() != 0) {
            this.items.add(null);
            notifyItemInserted(getItemCount() - 1);
            loading = true;
        }
    }

    public void resetListData() {
        this.items = new ArrayList<>();
        notifyDataSetChanged();
    }*/

   /* public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }*/
/*
    private void lastItemViewDetector(RecyclerView recyclerView) {
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int lastPos = layoutManager.findLastVisibleItemPosition();
                    if (!loading && lastPos == getItemCount() - 1 && onLoadMoreListener != null) {
                        if (onLoadMoreListener != null) {
                            int current_page = getItemCount() / Constant.PRODUCT_PER_REQUEST;
                            onLoadMoreListener.onLoadMore(current_page);
                        }
                        loading = true;
                    }
                }
            });
        }
    }*/
/*
    public interface OnLoadMoreListener {
        void onLoadMore(int current_page);
    }*/

}