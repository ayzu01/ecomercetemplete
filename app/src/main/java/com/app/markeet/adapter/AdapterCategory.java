package com.app.markeet.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.markeet.ActivityCategoryDetails;
import com.app.markeet.R;
import com.app.markeet.ThakaModels.CategoryModel;
import com.app.markeet.data.AppConfig;
import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;

import java.util.ArrayList;


public class AdapterCategory extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context ctx;
    private Activity activity;
    private ArrayList<CategoryModel.Body> items = new ArrayList<>();

    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, CategoryModel.Body obj);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView name;
        public TextView brief;
        public ImageView image;
        public LinearLayout lyt_color;
        public MaterialRippleLayout lyt_parent;

        public ViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.name);
            brief = (TextView) v.findViewById(R.id.brief);
            image = (ImageView) v.findViewById(R.id.image);
            lyt_color = (LinearLayout) v.findViewById(R.id.lyt_color);
            lyt_parent = (MaterialRippleLayout) v.findViewById(R.id.lyt_parent);
        }
    }

    public AdapterCategory(Context ctx, ArrayList<CategoryModel.Body> items,Activity activity) {
        this.ctx = ctx;
        this.items = items;
        this.activity=activity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
        vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder vItem = (ViewHolder) holder;
            final CategoryModel.Body c = items.get(position);
            vItem.name.setText(c.getTitle().getEnglish());
            vItem.brief.setText(c.getDescription().getEnglish());
            try {
                Glide.with(ctx).load(c.getImage().getThumbnail()).into(vItem.image);
                vItem.lyt_color.setBackgroundColor(Color.parseColor(c.getColor()));
              //   Tools.displayImageThumbnail(ctx, vItem.image, Constant.getURLimgCategory(c.getImage().getOriginal()), 0.5f);

            }catch (Exception e){
                e.printStackTrace();
            }
          //

            if (AppConfig.TINT_CATEGORY_ICON) {
              //  vItem.image.setColorFilter(Color.WHITE);
            }

            vItem.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    Toast.makeText(ctx, c.getTitle().getEnglish(), Toast.LENGTH_SHORT).show();
                    ActivityCategoryDetails.navigate(ctx, c);
                }
            });
            vItem.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(ctx, c.getTitle().getEnglish(), Toast.LENGTH_SHORT).show();
                    ActivityCategoryDetails.navigate(ctx, c);
                }
            });
            vItem.name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(ctx, c.getTitle().getEnglish(), Toast.LENGTH_SHORT).show();
                    ActivityCategoryDetails.navigate(ctx, c);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setItems(ArrayList<CategoryModel.Body> items) {
        this.items = items;
        notifyDataSetChanged();
    }


}