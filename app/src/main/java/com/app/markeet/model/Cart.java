package com.app.markeet.model;

import android.content.Intent;

public class Cart {

    public Long id;
    public Long order_id = -1L;
    public String product_id;
    public String product_name;
    public String image;
    public Integer amount = 0;
    public Integer stock ;
    public Double price_item;
    public long created_at;

    public Cart() {
    }

    public Cart(String product_id, String product_name, String image, Integer amount, Integer stock, Double price_item, long created_at) {
        this.product_id = product_id;
        this.product_name = product_name;
        this.image = image;
        this.amount = amount;
        this.stock = stock;
        this.price_item = price_item;
        this.created_at = created_at;
    }
}
