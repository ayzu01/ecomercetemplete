package com.app.markeet;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.markeet.ThakaAPIServices.APIClient;
import com.app.markeet.ThakaAPIServices.APIService;
import com.app.markeet.ThakaModels.SingleProductDetailModel;
import com.app.markeet.adapter.AdapterProductImage;
import com.app.markeet.adapter.ProductReviewsAdapter;
import com.app.markeet.connection.callbacks.CallbackProductDetails;
import com.app.markeet.data.AppConfig;
import com.app.markeet.data.DatabaseHandler;
import com.app.markeet.data.SharedPref;
import com.app.markeet.model.Cart;
import com.app.markeet.model.Wishlist;
import com.app.markeet.utils.NetworkCheck;
import com.balysv.materialripple.MaterialRippleLayout;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityProductDetails extends AppCompatActivity {

    private static final String EXTRA_OBJECT_ID = "key.EXTRA_OBJECT_ID";
    private static final String EXTRA_OBJECT_TITLE = "key.EXTRA_OBJECT_TITLE";
    private static final String EXTRA_FROM_NOTIF = "key.EXTRA_FROM_NOTIF";

    // activity transition
    public static void navigate(Context activity, String id,String title, Boolean from_notif) {
        Intent i = navigateBase(activity, id, title,from_notif);
        activity.startActivity(i);
    }

    public static Intent navigateBase(Context context, String id,String title, Boolean from_notif) {
        Intent i = new Intent(context, ActivityProductDetails.class);
        i.putExtra(EXTRA_OBJECT_ID, id);
        i.putExtra(EXTRA_OBJECT_TITLE, title);
        i.putExtra(EXTRA_FROM_NOTIF, from_notif);
        return i;
    }

    private String product_id;
    private Boolean from_notif;

    // extra obj
 //   private Product product;
    private SingleProductDetailModel product;
    private MenuItem wishlist_menu;
    private boolean flag_wishlist = false;
    private boolean flag_cart = false;
    private DatabaseHandler db;

    private Call<CallbackProductDetails> callbackCall = null;
    private Toolbar toolbar;
    private ActionBar actionBar;
    private TextView content_text;
    private SwipeRefreshLayout swipe_refresh;
    private MaterialRippleLayout lyt_add_cart;
    private TextView tv_add_cart;

    private SharedPref sharedPref;

    private ProductReviewsAdapter productAdapterReview;


    RecyclerView recyclerView_reviews;
    String title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        product_id = getIntent().getStringExtra(EXTRA_OBJECT_ID);
        title = getIntent().getStringExtra(EXTRA_OBJECT_TITLE);
        from_notif = getIntent().getBooleanExtra(EXTRA_FROM_NOTIF, false);

        db = new DatabaseHandler(this);
        sharedPref = new SharedPref(this);

        initToolbar();
        initComponent();
        requestAction();
        //prepareAds();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("");
    }

    private void initComponent() {
        content_text = findViewById(R.id.content_text);
        swipe_refresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        lyt_add_cart = (MaterialRippleLayout) findViewById(R.id.lyt_add_cart);
        tv_add_cart = (TextView) findViewById(R.id.tv_add_cart);
        recyclerView_reviews=findViewById(R.id.recyclerView_reviews);
        recyclerView_reviews.setHasFixedSize(true);
        recyclerView_reviews.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));


        // on swipe
        swipe_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestAction();
            }
        });

        lyt_add_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (product == null || (title != null && title.equals(""))) {
                    Toast.makeText(getApplicationContext(), R.string.please_wait_text, Toast.LENGTH_SHORT).show();
                    return;
                }
                toggleCartButton();
            }
        });

    }

    private void requestAction() {
        showFailedView(false, "");
        swipeProgress(true);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestNewsInfoDetailsApi();
            }
        }, 1000);
    }

    private void onFailRequest() {
        swipeProgress(false);
        if (NetworkCheck.isConnect(this)) {
            showFailedView(true, getString(R.string.failed_text));
        } else {
            showFailedView(true, getString(R.string.no_internet_text));
        }
    }


    private void prepareAds() {
      /*  if (AppConfig.ADS_PRODUCT_DETAILS && NetworkCheck.isConnect(getApplicationContext())) {
            MobileAds.initialize(getApplicationContext(), getString(R.string.banner_ad_unit_id));
            AdView mAdView = (AdView) findViewById(R.id.ad_view);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        } else {
            ((RelativeLayout) findViewById(R.id.banner_layout)).setVisibility(View.GONE);
        }*/
    }
    SingleProductDetailModel.Body body;
    private void requestNewsInfoDetailsApi() {
      /*  API api = RestAdapter.createAPI();
        callbackCall = api.getProductDetails(product_id);
        callbackCall.enqueue(new Callback<CallbackProductDetails>() {
            @Override
            public void onResponse(Call<CallbackProductDetails> call, Response<CallbackProductDetails> response) {
                CallbackProductDetails resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    product = resp.product;
                    displayPostData();
                    swipeProgress(false);
                } else {
                    onFailRequest();
                }
            }

            @Override
            public void onFailure(Call<CallbackProductDetails> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest();
            }
        });*/
        final TextView price = (TextView) findViewById(R.id.price);
        final TextView price_strike = (TextView) findViewById(R.id.price_strike);

        APIService service= APIClient.getClient().create(APIService.class);
        Call<SingleProductDetailModel> modelCall=service.SINGLE_PRODUCT_DETAIL_MODEL_CALL(String.valueOf(product_id));
        modelCall.enqueue(new Callback<SingleProductDetailModel>() {
            @Override
            public void onResponse(Call<SingleProductDetailModel> call, Response<SingleProductDetailModel> response) {
                if (response.isSuccessful()) {
                    try {
                         product= response.body();
                        if (product.getStatus()) {
                            swipe_refresh.setRefreshing(false);
                           body = product.getBody();

                            ArrayList<SingleProductDetailModel.Review> arrayList = new ArrayList<>();
                            arrayList = body.getReviews();
                            productAdapterReview = new ProductReviewsAdapter(ActivityProductDetails.this, arrayList);
                            recyclerView_reviews.setAdapter(productAdapterReview);
                            displayImageSlider(body.getImages());

                            ((TextView) findViewById(R.id.title)).setText(title);
                            if (body.getDiscount()>0){
                                price_strike.setText(body.getDiscount());
                                price_strike.setVisibility(View.VISIBLE);
                                price.setText(String.valueOf(body.getPrice()));
                            }else {
                                price_strike.setVisibility(View.GONE);
                                price.setVisibility(View.VISIBLE);
                                price.setText(String.valueOf(body.getPrice()));
                            }
                            content_text.setText(body.getDescription().getEnglish());
                            displayCategoryProduct();


                        } else {
                            Toast.makeText(ActivityProductDetails.this, "Network Error", Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<SingleProductDetailModel> call, Throwable t) {
                Toast.makeText(ActivityProductDetails.this, "Failed to get error about this task", Toast.LENGTH_SHORT).show();
            }
        });
    }

/*
    private void displayPostData() {


        webview = (WebView) findViewById(R.id.content);
        String html_data = "<style>img{max-width:100%;height:auto;} iframe{width:100%;}</style> ";
        html_data += product.getBody().getDescription().getEnglish();
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.setBackgroundColor(Color.TRANSPARENT);
        webview.setWebChromeClient(new WebChromeClient());
        webview.loadData(html_data, "text/html; charset=UTF-8", null);
        // disable scroll on touch
        webview.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });

     //   ((TextView) findViewById(R.id.date)).setText(Tools.getFormattedDate(product.last_update));

        TextView price = (TextView) findViewById(R.id.price);
        TextView price_strike = (TextView) findViewById(R.id.price_strike);

        // handle discount view
      */



/*  if (product.price_discount > 0) {
         *//*
*/
/*   price.setText(Tools.getFormattedPrice(product.price_discount, this));
            price_strike.setText(Tools.getFormattedPrice(product.price, this));*//*
*/
/*
            price_strike.setPaintFlags(price_strike.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            price_strike.setVisibility(View.VISIBLE);
        } else {
            //price.setText(Tools.getFormattedPrice(product.price, this));
            price_strike.setVisibility(View.GONE);
        }

        if (product.status.equalsIgnoreCase("READY STOCK")) {
            ((TextView) findViewById(R.id.status)).setText(getString(R.string.ready_stock));
        } else if (product.status.equalsIgnoreCase("OUT OF STOCK")) {
            ((TextView) findViewById(R.id.status)).setText(getString(R.string.out_of_stock));
        } else if (product.status.equalsIgnoreCase("SUSPEND")) {
            ((TextView) findViewById(R.id.status)).setText(getString(R.string.suspend));
        } else {
            ((TextView) findViewById(R.id.status)).setText(product.status);
        }
*//*

        // display Image slider


        // display category list at bottom

        Toast.makeText(this, R.string.msg_data_loaded, Toast.LENGTH_SHORT).show();

        // analytics track
      //  ThisApplication.getInstance().saveLogEvent(product.id, product.name, "PRODUCT_DETAILS");
    }
*/

    private void displayImageSlider(ArrayList<SingleProductDetailModel.Image> arrayList) {
        final LinearLayout layout_dots = (LinearLayout) findViewById(R.id.layout_dots);
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final AdapterProductImage adapterSlider = new AdapterProductImage(this, new ArrayList<SingleProductDetailModel.Image>());

         List<SingleProductDetailModel.Image> productImages = new ArrayList<>();
        productImages=arrayList;

       // if (product.product_images != null) productImages.addAll(product.product_images);
        adapterSlider.setItems(productImages);
        viewPager.setAdapter(adapterSlider);

        // displaying selected image first
        viewPager.setCurrentItem(0);
        addBottomDots(layout_dots, adapterSlider.getCount(), 0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int pos, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int pos) {
                addBottomDots(layout_dots, adapterSlider.getCount(), pos);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


        final ArrayList<String> images_list = new ArrayList<>();
        for (SingleProductDetailModel.Image img : productImages) {
            images_list.add(img.getOriginal());
        }

        adapterSlider.setOnItemClickListener(new AdapterProductImage.OnItemClickListener() {
            @Override
            public void onItemClick(View view, SingleProductDetailModel.Image obj, int pos) {
                Intent i = new Intent(ActivityProductDetails.this, ActivityFullScreenImage.class);
                i.putExtra(ActivityFullScreenImage.EXTRA_POS, pos);
                i.putStringArrayListExtra(ActivityFullScreenImage.EXTRA_IMGS, images_list);
                startActivity(i);
            }
        });
    }

    private void displayCategoryProduct() {
        TextView category = (TextView) findViewById(R.id.category);
        String html_data = "";
        for (int i = 0; i < product.getBody().getKeywords().size(); i++) {
            html_data += (i + 1) + ". " + product.getBody().getKeywords().get(i) + "\n";
        }
        category.setText(html_data);
    }

    private void addBottomDots(LinearLayout layout_dots, int size, int current) {
        ImageView[] dots = new ImageView[size];

        layout_dots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new ImageView(this);
            int width_height = 15;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(width_height, width_height));
            params.setMargins(10, 10, 10, 10);
            dots[i].setLayoutParams(params);
            dots[i].setImageResource(R.drawable.shape_circle);
            dots[i].setColorFilter(ContextCompat.getColor(this, R.color.darkOverlaySoft));
            layout_dots.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[current].setColorFilter(ContextCompat.getColor(this, R.color.colorPrimaryLight));
    }

    private void showFailedView(boolean show, String message) {
        View lyt_failed = (View) findViewById(R.id.lyt_failed);
        View lyt_main_content = (View) findViewById(R.id.lyt_main_content);

        ((TextView) findViewById(R.id.failed_message)).setText(message);
        if (show) {
            lyt_main_content.setVisibility(View.GONE);
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            lyt_main_content.setVisibility(View.VISIBLE);
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction();
            }
        });
    }

    private void swipeProgress(final boolean show) {
        if (!show) {
            swipe_refresh.setRefreshing(show);
            return;
        }
        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(show);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_product_details, menu);
        wishlist_menu = menu.findItem(R.id.action_wish);
        refreshWishlistMenu();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int item_id = item.getItemId();
        if (item_id == android.R.id.home) {
            onBackAction();
        } else if (item_id == R.id.action_wish) {
           /* if (title == null || title.equals("")) {
                Toast.makeText(this, R.string.cannot_add_wishlist, Toast.LENGTH_SHORT).show();
                return true;
            }
            if (flag_wishlist) {
                db.deleteWishlist(product_id);
                Toast.makeText(this, R.string.remove_wishlist, Toast.LENGTH_SHORT).show();
            } else {
                Random random=new Random(10000);
                long value=random.nextLong();

                Wishlist w = new Wishlist(value,product.getBody().getId(), title, product.getBody().getImages().get(0).getOriginal(), System.currentTimeMillis());
                db.saveWishlist(w);
                Toast.makeText(this, R.string.add_wishlist, Toast.LENGTH_SHORT).show();
            }
            refreshWishlistMenu();*/
        } else if (item_id == R.id.action_cart) {
            Intent i = new Intent(this, ActivityShoppingCart.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        onBackAction();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //if (webview != null) webview.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
       // if (webview != null) webview.onPause();
        refreshCartButton();
    }

    private void onBackAction() {
        if (from_notif) {
            if (ActivityMain.active) {
                finish();
            } else {
                Intent intent = new Intent(getApplicationContext(), ActivitySplash.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        } else {
            super.onBackPressed();
        }
    }

    private void refreshWishlistMenu() {
        Wishlist w = db.getWishlist(product_id);
        flag_wishlist = (w != null);
        if (flag_wishlist) {
            wishlist_menu.setIcon(R.drawable.ic_wish);
        } else {
            wishlist_menu.setIcon(R.drawable.ic_wish_outline);
        }
    }

    private void toggleCartButton() {
        if (flag_cart) {
            db.deleteActiveCart(product_id);
            Toast.makeText(this, R.string.remove_cart, Toast.LENGTH_SHORT).show();
        } else {
            // check stock product
          /*  if (product.stock == 0 || product.status.equalsIgnoreCase("OUT OF STOCK")) {
                Toast.makeText(this, R.string.msg_out_of_stock, Toast.LENGTH_SHORT).show();
                return;
            }
            if (product.status.equalsIgnoreCase("SUSPEND")) {
                Toast.makeText(this, R.string.msg_suspend, Toast.LENGTH_SHORT).show();
                return;
            }*/
          //  Double selected_price = Double.valueOf(product.getBody().getPrice() > 0 ? product.getBody().getDiscount() : product.getBody().getPrice());
            Double selected_price= (double) (body.getPrice() - body.getDiscount());

            Cart cart = new Cart(product.getBody().getId(), title, product.getBody().getImages().get(0).getOriginal(), 1, 1, selected_price, System.currentTimeMillis());
            db.saveCart(cart);
            Toast.makeText(this, R.string.add_cart, Toast.LENGTH_SHORT).show();
        }
        refreshCartButton();
    }

    private void refreshCartButton() {
        Cart c = db.getCart(product_id);
        flag_cart = (c != null);
        if (flag_cart) {
            lyt_add_cart.setBackgroundColor(getResources().getColor(R.color.colorRemoveCart));
            tv_add_cart.setText(R.string.bt_remove_cart);
        } else {
            lyt_add_cart.setBackgroundColor(getResources().getColor(R.color.colorAddCart));
            tv_add_cart.setText(R.string.bt_add_cart);
        }
    }
}
