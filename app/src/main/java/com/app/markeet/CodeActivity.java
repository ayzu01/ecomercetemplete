package com.app.markeet;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.markeet.ThakaAPIServices.APIClient;
import com.app.markeet.ThakaAPIServices.APIService;
import com.app.markeet.ThakaModels.SignupModel;
import com.app.markeet.ThakaPreferences.Preferences;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;

import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CodeActivity extends AppCompatActivity {
    Bundle bundle ;
    Intent intent;
    private FirebaseAuth mAuth;
    private static final String TAG = "PhoneAuthActivity";

    private static final int STATE_INITIALIZED = 1;
    private static final int STATE_CODE_SENT = 2;
    private static final int STATE_VERIFY_FAILED = 3;
    private static final int STATE_VERIFY_SUCCESS = 4;
    private static final int STATE_SIGNIN_FAILED = 5;
    private static final int STATE_SIGNIN_SUCCESS = 6;
    String number,email,name,veri;
    ProgressDialog progressDialog;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    private boolean mVerificationInProgress = false;
    private String mVerificationId;

 //   private EditText ;
    int OTP_STATUS=0;
    Preferences preferences;
    TextView number_text,resend_code;
    private EditText mVerificationField;
    String otp_number;

    Button ok_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code);


        progressDialog = new ProgressDialog(CodeActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(getResources().getString(R.string.please_wait));

        preferences=new Preferences(CodeActivity.this);

        number  = getIntent().getExtras().getString("phn_number","+9230365481");
        email = getIntent().getExtras().getString("email");
        name = getIntent().getExtras().getString("name");
        OTP_STATUS = getIntent().getExtras().getInt("otp_status");
        veri = getIntent().getExtras().getString("verificationId");
        number = getIntent().getExtras().getString("phn_number");
        mResendToken = (PhoneAuthProvider.ForceResendingToken) getIntent().getExtras().getParcelable("token");

        number_text=findViewById(R.id.number);
        resend_code=findViewById(R.id.resend_code);
        number_text.setText(number);



        mVerificationField = findViewById(R.id.code_txt);


        ok_btn=findViewById(R.id.ok_btn);
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otp_number = mVerificationField.getText().toString();
                if (TextUtils.isEmpty(otp_number) && otp_number.equalsIgnoreCase("")) {
                    Toast.makeText(CodeActivity.this, "No Code Enter", Toast.LENGTH_SHORT).show();
                } else {
                    verifyPhoneNumberWithCode(veri, otp_number);
                }
            }
        });

     //   mVerificationField = (EditText) findViewById(R.id.code_txt);
        /* bundle.putString("verificationId", s);
                bundle.putParcelable("token", mResendToken);
                bundle.putString("phn_number", number);
                bundle.putString("email",email.getText().toString() );
                bundle.putString("name", name.getText().toString());
                bundle.putInt("otp_status", 3);*/


        mAuth = FirebaseAuth.getInstance();
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                progressDialog.dismiss();
                Log.d(TAG, "onVerificationCompleted:" + credential);
                mVerificationInProgress = false;
                updateUI(STATE_VERIFY_SUCCESS, credential);
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                progressDialog.dismiss();
                Log.w(TAG, "onVerificationFailed", e);
                mVerificationInProgress = false;

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.quotaExcceded),
                            Snackbar.LENGTH_SHORT).show();
                }
                updateUI(STATE_VERIFY_FAILED);
            }

                @Override
                public void onCodeSent(String verificationId,
                        PhoneAuthProvider.ForceResendingToken token) {
                    progressDialog.dismiss();
                    Log.d(TAG, "onCodeSent:" + verificationId);
                    mVerificationId = verificationId;
                    mResendToken = token;
                    updateUI(STATE_CODE_SENT);
                }
            };
        resend_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resendVerificationCode(number,mResendToken);
            }
        });

    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(credential);
    }

    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,
                30,
                TimeUnit.SECONDS,
                CodeActivity.this,
                mCallbacks,
                token);
        Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.codeResended),
                Snackbar.LENGTH_SHORT).show();
    }


    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        // progressDoalog.show();
        mAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    progressDialog.show();
                    Log.d(TAG, "signInWithCredential:success");

                    FirebaseUser user = task.getResult().getUser();

                    updateUI(STATE_SIGNIN_SUCCESS, user);
                } else {
                    Log.w(TAG, "signInWithCredential:failure", task.getException());
                    if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                        mVerificationField.setError(getResources().getString(R.string.invalidCode));
                    }
                    updateUI(STATE_SIGNIN_FAILED);
                }
            }
        });
    }

    private void updateUI(int uiState) {
        updateUI(uiState, mAuth.getCurrentUser(), null);
    }


    private void updateUI(int uiState, FirebaseUser user) {
        updateUI(uiState, user, null);

        CallAPISaving();

    }

    private void CallAPISaving() {
        APIService service= APIClient.getClient().create(APIService.class);
        final Call<SignupModel> modelCall=service.Signup(email,name,number);
        modelCall.enqueue(new Callback<SignupModel>() {
            @Override
            public void onResponse(Call<SignupModel> call, Response<SignupModel> response) {
                if (response.isSuccessful()){
                    progressDialog.dismiss();
                    SignupModel model=response.body();
                    if (model.getStatus()) {
                        Toast.makeText(CodeActivity.this, model.getMessage().getEnglish(), Toast.LENGTH_SHORT).show();
                        preferences.setID(model.getBody().getId());
                        preferences.setEmail(model.getBody().getEmail());
                        preferences.setNumber(model.getBody().getNumber());
                        preferences.setName(model.getBody().getName());
                        preferences.setSession(true);
                          startActivity(new Intent(getApplicationContext(),ActivityMain.class));
                         finish();
                    }else {
                  //      progressDialog.dismiss();
                        Toast.makeText(CodeActivity.this, model.getMessage().getEnglish(), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    progressDialog.dismiss();
                    Toast.makeText(CodeActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SignupModel> call, Throwable t) {
                Toast.makeText(CodeActivity.this, "Network Error", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
                Log.e(TAG, "onFailure: "+t.getStackTrace());
            }
        });
    }

    private void updateUI(int uiState, PhoneAuthCredential cred) {
        updateUI(uiState, null, cred);
    }

    private void updateUI(int uiState, FirebaseUser user, PhoneAuthCredential cred) {
        switch (uiState) {
            case STATE_INITIALIZED:
                break;
            case STATE_CODE_SENT:
                break;
            case STATE_VERIFY_FAILED:
                break;
            case STATE_VERIFY_SUCCESS:

                if (cred != null) {
                    if (cred.getSmsCode() != null) {
                        mVerificationField.setText(cred.getSmsCode());
                    } else {
                        mVerificationField.setText(getResources().getString(R.string.instantValidation));
                    }
                }

                break;
            case STATE_SIGNIN_FAILED:
                break;
            case STATE_SIGNIN_SUCCESS:
                break;
        }

        if (user == null) {
        } else {
            mVerificationField.setText(null);
        }
    }

    public void my(View view){
        String code = mVerificationField.getText().toString();
        if (TextUtils.isEmpty(code)) {
            mVerificationField.setError(getResources().getString(R.string.notEmptyWarning));
            return;
        }

        verifyPhoneNumberWithCode(veri, code);
    }
    public void verifyCode(View view){
        resendVerificationCode(number, mResendToken);

    }
}
