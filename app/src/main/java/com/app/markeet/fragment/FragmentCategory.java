package com.app.markeet.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.app.markeet.ActivityMain;
import com.app.markeet.R;
import com.app.markeet.ThakaAPIServices.APIClient;
import com.app.markeet.ThakaAPIServices.APIService;
import com.app.markeet.ThakaModels.CategoryModel;
import com.app.markeet.adapter.AdapterCategory;
import com.app.markeet.utils.NetworkCheck;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.markeet.ActivityMain.search_layout_visible;
import static com.app.markeet.ActivityMain.search_view_edit;

public class FragmentCategory extends Fragment {

    private View root_view;
    private RecyclerView recyclerView;
 //   private Call<CallbackCategory> callbackCall;
    private AdapterCategory adapter;
    ArrayList<CategoryModel.Body> arrayList=new ArrayList<>();
    Context context;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.fragment_category, null);
        context=container.getContext();
        initComponent();
        requestListCategory(root_view);

        return root_view;
    }

    private void initComponent() {
        recyclerView = (RecyclerView) root_view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1));

        //set data and list adapter

        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setVisibility(View.GONE);


        ActivityMain.search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  ActivitySearch.navigate(ActivityMain.this);
                if (ActivityMain.search_layout_visible){
                    ActivityMain. toolbar.setVisibility(View.VISIBLE);
                    search_view_edit.setVisibility(View.GONE);
                    search_layout_visible=false;
                    search_view_edit.setText("");
                    callSearchAPI(search_view_edit.getText().toString());
                }else {
                    ActivityMain.toolbar.setVisibility(View.GONE);
                    search_layout_visible=true;
                    search_view_edit.setVisibility(View.VISIBLE);
                }



            }
        });

        search_view_edit.setOnKeyListener(new View.OnKeyListener()
        {
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if (event.getAction() == KeyEvent.ACTION_DOWN)
                {
                    switch (keyCode)
                    {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            ActivityMain.toolbar.setVisibility(View.VISIBLE);
                            search_view_edit.setVisibility(View.GONE);
                            search_layout_visible=true;
                            search_view_edit.setText("");
                            callSearchAPI(search_view_edit.getText().toString());
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });

        /*adapter.setOnItemClickListener(new AdapterCategory.OnItemClickListener() {
            @Override
            public void onItemClick(View view, CategoryModel.Body obj) {

            }
        });*/
    }

    private void callSearchAPI(String search_data) {
        arrayList.clear();
        adapter.notifyDataSetChanged();

        if (TextUtils.isEmpty(search_data) && search_data.equalsIgnoreCase("")){
            Toast.makeText(context, "Nothing Enter to search", Toast.LENGTH_SHORT).show();
        }else {
            APIService service= APIClient.getClient().create(APIService.class);

            Call<CategoryModel> modelCall=service.SearchCategory(search_data);
            modelCall.enqueue(new Callback<CategoryModel>() {
                @Override
                public void onResponse(Call<CategoryModel> call, Response<CategoryModel> response) {
                    try {
                        if (response.isSuccessful()) {
                            CategoryModel model = response.body();
                            arrayList=model.getBody();
                            if (arrayList.isEmpty() || arrayList.size()==0) {
                                Snackbar.make(root_view,"No data found",Snackbar.LENGTH_SHORT);
                            } else {

                                adapter = new AdapterCategory(context, arrayList,getActivity());
                                recyclerView.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                                recyclerView.setVisibility(View.VISIBLE);
                                ActivityMain.getInstance().category_load = true;
                                ActivityMain.getInstance().showDataLoaded();
                            }
                        }else {
                            Log.e("error", "onResponse: " + response.errorBody());
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<CategoryModel> call, Throwable t) {
                    ActivityMain.getInstance().category_load = true;
                    ActivityMain.getInstance().showDataLoaded();
                    Toast.makeText(context, "Network Error", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void requestListCategory(final View root_view) {
      /*  API api = RestAdapter.createAPI();
        callbackCall = api.getListCategory();
        callbackCall.enqueue(new Callback<CallbackCategory>() {
            @Override
            public void onResponse(Call<CallbackCategory> call, Response<CallbackCategory> response) {
                CallbackCategory resp = response.body();
                if (resp != null && resp.status.equals("success")) {
                    recyclerView.setVisibility(View.VISIBLE);
                    adapter.setItems(resp.categories);
                    ActivityMain.getInstance().category_load = true;
                    ActivityMain.getInstance().showDataLoaded();
                } else {
                    onFailRequest();
                }
            }

            @Override
            public void onFailure(Call<CallbackCategory> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                if (!call.isCanceled()) onFailRequest();
            }

        });*/

        APIService service=APIClient.getClient().create(APIService.class);
        Call<CategoryModel> modelCall=service.Category();
        modelCall.enqueue(new Callback<CategoryModel>() {
            @Override
            public void onResponse(Call<CategoryModel> call, Response<CategoryModel> response) {
                try {
                    if (response.isSuccessful()) {
                        CategoryModel model = response.body();
                        arrayList=model.getBody();
                        if (arrayList.isEmpty()) {
                            Snackbar.make(root_view,"No data found",Snackbar.LENGTH_SHORT);
                        } else {
                            adapter = new AdapterCategory(context, arrayList,getActivity());
                            recyclerView.setAdapter(adapter);
                            recyclerView.setVisibility(View.VISIBLE);
                            ActivityMain.getInstance().category_load = true;
                            ActivityMain.getInstance().showDataLoaded();
                        }
                    }else {
                        Log.e("error", "onResponse: " + response.errorBody());
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CategoryModel> call, Throwable t) {
                Toast.makeText(context, "Network Error", Toast.LENGTH_SHORT).show();
                ActivityMain.getInstance().category_load = true;
                ActivityMain.getInstance().showDataLoaded();
            }
        });

    }

    private void onFailRequest() {
        if (NetworkCheck.isConnect(getActivity())) {
            showFailedView(R.string.msg_failed_load_data);
        } else {
            showFailedView(R.string.no_internet_text);
        }
    }

    private void showFailedView(@StringRes int message) {
        ActivityMain.getInstance().showDialogFailed(message);
    }

}
