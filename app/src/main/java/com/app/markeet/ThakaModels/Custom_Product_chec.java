package com.app.markeet.ThakaModels;

public class Custom_Product_chec {
    String product_id;
    Integer quantity;

    public Custom_Product_chec(String product_id, Integer amount) {
        this.product_id = product_id;
        this.quantity=amount;
    }

    public String getProduct_id() {
        return product_id;
    }

    public Integer getQuantity() {
        return quantity;
    }
}
