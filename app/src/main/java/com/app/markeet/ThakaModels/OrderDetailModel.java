package com.app.markeet.ThakaModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderDetailModel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("body")
    @Expose
    private Body body;
    @SerializedName("message")
    @Expose
    private Message message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }


    public class Body {

        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("products")
        @Expose
        private List<Product> products = null;
        @SerializedName("packages")
        @Expose
        private List<Object> packages = null;
        @SerializedName("customer")
        @Expose
        private Customer customer;
        @SerializedName("total_amount")
        @Expose
        private Integer totalAmount;

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public List<Product> getProducts() {
            return products;
        }

        public void setProducts(List<Product> products) {
            this.products = products;
        }

        public List<Object> getPackages() {
            return packages;
        }

        public void setPackages(List<Object> packages) {
            this.packages = packages;
        }

        public Customer getCustomer() {
            return customer;
        }

        public void setCustomer(Customer customer) {
            this.customer = customer;
        }

        public Integer getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(Integer totalAmount) {
            this.totalAmount = totalAmount;
        }

    }


    public class Customer {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    public class Image {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("original")
        @Expose
        private String original;
        @SerializedName("thumbnail")
        @Expose
        private String thumbnail;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOriginal() {
            return original;
        }

        public void setOriginal(String original) {
            this.original = original;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }

    }

    public class Message {

        @SerializedName("english")
        @Expose
        private String english;
        @SerializedName("urdu")
        @Expose
        private String urdu;

        public String getEnglish() {
            return english;
        }

        public void setEnglish(String english) {
            this.english = english;
        }

        public String getUrdu() {
            return urdu;
        }

        public void setUrdu(String urdu) {
            this.urdu = urdu;
        }

    }

    public class Product {

        @SerializedName("quantity")
        @Expose
        private Integer quantity;
        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("product")
        @Expose
        private Product_ product;

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Product_ getProduct() {
            return product;
        }

        public void setProduct(Product_ product) {
            this.product = product;
        }

    }

    public class Product_ {

        @SerializedName("title")
        @Expose
        private Title title;
        @SerializedName("discount")
        @Expose
        private Integer discount;
        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("images")
        @Expose
        private List<Image> images = null;
        @SerializedName("price")
        @Expose
        private Integer price;
        @SerializedName("rating")
        @Expose
        private Integer rating;

        public Title getTitle() {
            return title;
        }

        public void setTitle(Title title) {
            this.title = title;
        }

        public Integer getDiscount() {
            return discount;
        }

        public void setDiscount(Integer discount) {
            this.discount = discount;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public List<Image> getImages() {
            return images;
        }

        public void setImages(List<Image> images) {
            this.images = images;
        }

        public Integer getPrice() {
            return price;
        }

        public void setPrice(Integer price) {
            this.price = price;
        }

        public Integer getRating() {
            return rating;
        }

        public void setRating(Integer rating) {
            this.rating = rating;
        }

    }

    public class Title {

        @SerializedName("english")
        @Expose
        private String english;
        @SerializedName("urdu")
        @Expose
        private String urdu;

        public String getEnglish() {
            return english;
        }

        public void setEnglish(String english) {
            this.english = english;
        }

        public String getUrdu() {
            return urdu;
        }

        public void setUrdu(String urdu) {
            this.urdu = urdu;
        }

    }
}
