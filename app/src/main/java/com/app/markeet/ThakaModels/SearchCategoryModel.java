package com.app.markeet.ThakaModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SearchCategoryModel {
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("body")
    @Expose
    private ArrayList<Body> body = null;
    @SerializedName("message")
    @Expose
    private Message message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ArrayList<Body> getBody() {
        return body;
    }

    public void setBody(ArrayList<Body> body) {
        this.body = body;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
    public class Body {

        @SerializedName("title")
        @Expose
        private Title title;
        @SerializedName("description")
        @Expose
        private Description description;
        @SerializedName("image")
        @Expose
        private Image image;
        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("color")
        @Expose
        private String color;
        @SerializedName("__v")
        @Expose
        private Integer v;

        public Title getTitle() {
            return title;
        }

        public void setTitle(Title title) {
            this.title = title;
        }

        public Description getDescription() {
            return description;
        }

        public void setDescription(Description description) {
            this.description = description;
        }

        public Image getImage() {
            return image;
        }

        public void setImage(Image image) {
            this.image = image;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

    }
    public class Description {

        @SerializedName("english")
        @Expose
        private String english;
        @SerializedName("urdu")
        @Expose
        private String urdu;

        public String getEnglish() {
            return english;
        }

        public void setEnglish(String english) {
            this.english = english;
        }

        public String getUrdu() {
            return urdu;
        }

        public void setUrdu(String urdu) {
            this.urdu = urdu;
        }

    }

    public class Image {

        @SerializedName("original")
        @Expose
        private String original;
        @SerializedName("thumbnail")
        @Expose
        private String thumbnail;

        public String getOriginal() {
            return original;
        }

        public void setOriginal(String original) {
            this.original = original;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }

    }

    public class Message {

        @SerializedName("english")
        @Expose
        private String english;
        @SerializedName("urdu")
        @Expose
        private String urdu;

        public String getEnglish() {
            return english;
        }

        public void setEnglish(String english) {
            this.english = english;
        }

        public String getUrdu() {
            return urdu;
        }

        public void setUrdu(String urdu) {
            this.urdu = urdu;
        }

    }

    public class Title {

        @SerializedName("english")
        @Expose
        private String english;
        @SerializedName("urdu")
        @Expose
        private String urdu;

        public String getEnglish() {
            return english;
        }

        public void setEnglish(String english) {
            this.english = english;
        }

        public String getUrdu() {
            return urdu;
        }

        public void setUrdu(String urdu) {
            this.urdu = urdu;
        }

    }
}
