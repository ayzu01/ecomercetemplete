package com.app.markeet.ThakaModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderAPIMOdel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private Message message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }


    public class Message {

        @SerializedName("english")
        @Expose
        private String english;
        @SerializedName("urdu")
        @Expose
        private String urdu;

        public String getEnglish() {
            return english;
        }

        public void setEnglish(String english) {
            this.english = english;
        }

        public String getUrdu() {
            return urdu;
        }

        public void setUrdu(String urdu) {
            this.urdu = urdu;
        }

    }
}