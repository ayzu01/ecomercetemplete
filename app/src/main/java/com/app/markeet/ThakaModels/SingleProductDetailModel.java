package com.app.markeet.ThakaModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SingleProductDetailModel {



    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("body")
    @Expose
    private Body body;
    @SerializedName("message")
    @Expose
    private Message message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }


    public class Body {

        @SerializedName("description")
        @Expose
        private Description description;
        @SerializedName("color")
        @Expose
        private Color color;
        @SerializedName("keywords")
        @Expose
        private ArrayList<String> keywords = null;
        @SerializedName("discount")
        @Expose
        private Integer discount;
        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("images")
        @Expose
        private ArrayList<Image> images = null;
        @SerializedName("price")
        @Expose
        private Integer price;
        @SerializedName("__v")
        @Expose
        private Integer v;
        @SerializedName("reviews")
        @Expose
        private ArrayList<Review> reviews = null;
        @SerializedName("rating")
        @Expose
        private Integer rating;

        public Description getDescription() {
            return description;
        }

        public void setDescription(Description description) {
            this.description = description;
        }

        public Color getColor() {
            return color;
        }

        public void setColor(Color color) {
            this.color = color;
        }

        public ArrayList<String> getKeywords() {
            return keywords;
        }

        public void setKeywords(ArrayList<String> keywords) {
            this.keywords = keywords;
        }

        public Integer getDiscount() {
            return discount;
        }

        public void setDiscount(Integer discount) {
            this.discount = discount;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public ArrayList<Image> getImages() {
            return images;
        }

        public void setImages(ArrayList<Image> images) {
            this.images = images;
        }

        public Integer getPrice() {
            return price;
        }

        public void setPrice(Integer price) {
            this.price = price;
        }

        public Integer getV() {
            return v;
        }

        public void setV(Integer v) {
            this.v = v;
        }

        public ArrayList<Review> getReviews() {
            return reviews;
        }

        public void setReviews(ArrayList<Review> reviews) {
            this.reviews = reviews;
        }

        public Integer getRating() {
            return rating;
        }

        public void setRating(Integer rating) {
            this.rating = rating;
        }

    }

    public class Color {

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("code")
        @Expose
        private String code;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

    }

    public class Review {

        @SerializedName("addedOn")
        @Expose
        private String addedOn;
        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("body")
        @Expose
        private String body;
        @SerializedName("rating")
        @Expose
        private Integer rating;
        @SerializedName("customer")
        @Expose
        private Customer customer;

        public String getAddedOn() {
            return addedOn;
        }

        public void setAddedOn(String addedOn) {
            this.addedOn = addedOn;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public Integer getRating() {
            return rating;
        }

        public void setRating(Integer rating) {
            this.rating = rating;
        }

        public Customer getCustomer() {
            return customer;
        }

        public void setCustomer(Customer customer) {
            this.customer = customer;
        }


    }

    public class Customer {

        @SerializedName("name")
        @Expose
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    public class Description {

        @SerializedName("english")
        @Expose
        private String english;
        @SerializedName("urdu")
        @Expose
        private String urdu;

        public String getEnglish() {
            return english;
        }

        public void setEnglish(String english) {
            this.english = english;
        }

        public String getUrdu() {
            return urdu;
        }

        public void setUrdu(String urdu) {
            this.urdu = urdu;
        }

    }

    public class Image {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("original")
        @Expose
        private String original;
        @SerializedName("thumbnail")
        @Expose
        private String thumbnail;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOriginal() {
            return original;
        }

        public void setOriginal(String original) {
            this.original = original;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }

    }

    public class Message {

        @SerializedName("english")
        @Expose
        private String english;
        @SerializedName("urdu")
        @Expose
        private String urdu;

        public String getEnglish() {
            return english;
        }

        public void setEnglish(String english) {
            this.english = english;
        }

        public String getUrdu() {
            return urdu;
        }

        public void setUrdu(String urdu) {
            this.urdu = urdu;
        }
    }
}
