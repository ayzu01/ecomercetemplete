package com.app.markeet.ThakaModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ProductFeatureModel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("body")
    @Expose
    private ArrayList<Body> body = null;
    @SerializedName("message")
    @Expose
    private Message message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ArrayList<Body> getBody() {
        return body;
    }

    public void setBody(ArrayList<Body> body) {
        this.body = body;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }


    public class Body {

        @SerializedName("title")
        @Expose
        private Title title;
        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("images")
        @Expose
        private ArrayList<Image> images = null;
        @SerializedName("rating")
        @Expose
        private Integer rating;
        @SerializedName("price")
        @Expose
        private Integer price;
        @SerializedName("discount")
        @Expose
        private Integer discount;
        @SerializedName("description")
        @Expose
        private Description description;

        public Title getTitle() {
            return title;
        }

        public void setTitle(Title title) {
            this.title = title;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public ArrayList<Image> getImages() {
            return images;
        }

        public void setImages(ArrayList<Image> images) {
            this.images = images;
        }

        public Integer getRating() {
            return rating;
        }

        public void setRating(Integer rating) {
            this.rating = rating;
        }

        public Integer getPrice() {
            return price;
        }

        public void setPrice(Integer price) {
            this.price = price;
        }

        public Integer getDiscount() {
            return discount;
        }

        public void setDiscount(Integer discount) {
            this.discount = discount;
        }

        public Description getDescription() {
            return description;
        }

        public void setDescription(Description description) {
            this.description = description;
        }

    }

    public class Description {

        @SerializedName("english")
        @Expose
        private String english;
        @SerializedName("urdu")
        @Expose
        private String urdu;

        public String getEnglish() {
            return english;
        }

        public void setEnglish(String english) {
            this.english = english;
        }

        public String getUrdu() {
            return urdu;
        }

        public void setUrdu(String urdu) {
            this.urdu = urdu;
        }

    }


    public class Image {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("original")
        @Expose
        private String original;
        @SerializedName("thumbnail")
        @Expose
        private String thumbnail;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOriginal() {
            return original;
        }

        public void setOriginal(String original) {
            this.original = original;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }

    }

    public class Message {

        @SerializedName("english")
        @Expose
        private String english;
        @SerializedName("urdu")
        @Expose
        private String urdu;

        public String getEnglish() {
            return english;
        }

        public void setEnglish(String english) {
            this.english = english;
        }

        public String getUrdu() {
            return urdu;
        }

        public void setUrdu(String urdu) {
            this.urdu = urdu;
        }

    }

    public class Title {

        @SerializedName("english")
        @Expose
        private String english;
        @SerializedName("urdu")
        @Expose
        private String urdu;

        public String getEnglish() {
            return english;
        }

        public void setEnglish(String english) {
            this.english = english;
        }

        public String getUrdu() {
            return urdu;
        }

        public void setUrdu(String urdu) {
            this.urdu = urdu;
        }

    }
}
