package com.app.markeet;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.markeet.ThakaAPIServices.APIClient;
import com.app.markeet.ThakaAPIServices.APIService;
import com.app.markeet.ThakaModels.LoginModel;
import com.app.markeet.ThakaPreferences.Preferences;
import com.google.android.gms.common.internal.Constants;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.hbb20.CountryCodePicker;

import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {


    TextView new_user;
    Button continue_button;
    CountryCodePicker getNumber;
    EditText mobilenumber;
    Preferences preference;
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.main_color));
        }

        setContentView(R.layout.activity_login);
        continue_button=findViewById(R.id.continue_button);

        dialog=new ProgressDialog(this);
        dialog.setMessage("Please wait");
        dialog.setTitle("Login ");
        dialog.setCancelable(false);

        preference=new Preferences(this);

        mobilenumber = findViewById(R.id.mobilenumber);
        getNumber = findViewById(R.id.getNumber);
        getNumber.registerCarrierNumberEditText(mobilenumber);

        new_user=findViewById(R.id.new_user);
       // intent = new Intent(getApplicationContext(), CodeScreen.class);
        new_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),SignupClass.class));
                finish();
            }
        });


        continue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number=getNumber.getFullNumberWithPlus();
                if (TextUtils.isEmpty(number) && number.equalsIgnoreCase("")){
                    Toast.makeText(LoginActivity.this, "Number Field should not be Empty", Toast.LENGTH_SHORT).show();
                }else {
                    CallAPI(number);
                }
            }
        });


    }

    private void CallAPI(String  getNumber) {
        dialog.show();
        APIService service= APIClient.getClient().create(APIService.class);
        Call<LoginModel> modelCall=service.LOGIN(getNumber);
        modelCall.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                if (response.isSuccessful()) {
                    dialog.dismiss();
                    LoginModel model = response.body();
                    if (model.getStatus()) {
                        Toast.makeText(LoginActivity.this, model.getMessage().getEnglish(), Toast.LENGTH_SHORT).show();
                        preference.setSession(true);
                        preference.setID(model.getBody().getId());
                        startActivity(new Intent(getApplicationContext(),ActivityMain.class));
                        finish();
                    }else {
                        Toast.makeText(LoginActivity.this, model.getMessage().getEnglish(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                dialog.dismiss();
                Log.e("eror", "onFailure: "+t.getStackTrace() );
                Toast.makeText(LoginActivity.this, "Network Error", Toast.LENGTH_SHORT).show();
            }
        });
    }


}
